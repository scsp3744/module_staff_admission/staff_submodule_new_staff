var express = require('express');
var router = express.Router();
const ctrlMain = require('../controllers/main');
const ctrlPayslip = require('../controllers/payslip')

router.get('/', ctrlPayslip.viewStaffPage);

router.route('/generate')
    .get(ctrlPayslip.generatePayslipPage)
    .post(ctrlPayslip.doGeneratePayslip)

module.exports = router;
