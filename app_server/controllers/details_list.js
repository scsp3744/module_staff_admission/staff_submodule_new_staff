const request = require('request');
const async = require('async');
const apiOptions = require('../../api_options')

const userDetailsRequest = {
    nation_list: (next) => {
        const path = '/nation';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    local_state_list: (next) => {
        const path = '/local_state';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    gender_list: (next) => {
        const path = '/gender';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    religion_list: (next) => {
        const path = '/religion';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    race_list: (next) => {
        const path = '/race';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    marriage_status_list: (next) => {
        const path = '/marriage_status';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    employee_status_list: (next) => {
        const path = '/employee_status';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    staff_position_list: (next) => {
        const path = '/staff_position';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    office_dept_list: (next) => {
        const path = '/office_dept';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
}

const selectedStaffDetailsRequest = {
    nation: (next) => {
        const path = '/nation';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    local_state: (next) => {
        const path = '/local_state';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    gender: (next) => {
        const path = '/gender';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    religion: (next) => {
        const path = '/religion';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    race: (next) => {
        const path = '/race';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    marriage_status: (next) => {
        const path = '/marriage_status';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    employee_status: (next) => {
        const path = '/employee_status';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    staff_position: (next) => {
        const path = '/staff_position';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
    office_dept: (next) => {
        const path = '/office_dept';
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                if (response.statusCode === 404) {
                    body = null;
                }
                next(null, body);
            }
        );
    },
}

module.exports = {
    userDetailsRequest,
    selectedStaffDetailsRequest
}