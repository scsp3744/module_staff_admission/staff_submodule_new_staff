const request = require('request');
const async = require('async');
const apiOptions = require('../../api_options')

/* GET home page. */
const viewStaffPage = (req, res) => {
    const path = '/payslip';
    const requestOptions = {
        url: `${apiOptions.server}${path}`,
        method: 'GET',
        json: {},
    };
    request(
        requestOptions,
        (err, response, body) => {
            if (response.statusCode === 404) {
                body = null;
            }
            res.render('payslip/view_staff', 
            { 
                title: 'View Staff',
                payslips: body
            });
        }
    );
};

const generatePayslipPage = (req, res) => {
    const path = '/staffs_with_latest_contract';
    const requestOptions = {
        url: `${apiOptions.server}${path}`,
        method: 'GET',
        json: {},
    };
    request(
        requestOptions,
        (err, response, body) => {
            if (response.statusCode === 404) {
                body = null;
                res.redirect('/payslip')
            }
            res.render('payslip/generate', 
            { 
                title: 'New Payslip',
                staffs: body
            });
        }
    );
}

const doGeneratePayslip = (req, res) => {
    let postdata = [];
    const payslips = req.body.payslips;
    console.log(payslips)
    for (let i = 0; i < payslips.length; i++) {
        let temp = {
            staff_id: payslips[i]['staff_id'],
            basic: payslips[i]['basic'],
            date_issued: new Date(),
            additional_items: null
        };
        console.log(temp)
        postdata.push(temp);
    }

    const path = '/payslip';
    const requestOptions = {
        url: `${apiOptions.server}${path}`,
        method: 'POST',
        json: postdata
    };
    request(
        requestOptions,
        (err, response, body) => {
            if (response.statusCode === 201) {
                res.redirect('/payslip');
            }
            else {
                showError(req, res, response.statusCode);
            }
        }
    );
};

module.exports = {
    viewStaffPage,
    generatePayslipPage,
    doGeneratePayslip
};